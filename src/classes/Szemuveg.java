
package classes;

public class Szemuveg {
    private Double dioptria;
    private Double cilinder;
    private Integer tengely;
    public final Integer ar = 50000;
    public Boolean fenyre_sotetedik;

    public Szemuveg(){
        this.dioptria = 0.0;
        this.cilinder = 0.0;
        this.tengely = 0;
        this.fenyre_sotetedik = false;
    }
    public Szemuveg(Double dioptria, Double cilinder, Integer tengely, Boolean fenyre_sotetedik){
        if(dioptria <= 10.0 && dioptria >= -10.0){
            this.dioptria = dioptria;
        }else{
            this.dioptria = 0.0;
        }
        if(cilinder <= 10.0 && cilinder >= -10.0){
            this.cilinder = cilinder;
        }else{
            this.cilinder = 0.0;
        }
        if(tengely >= 0 && tengely <= 180){
            this.tengely = tengely;
        }else{
            this.tengely = 0;
        }
        this.fenyre_sotetedik = fenyre_sotetedik;
    }
    
    
    public Double getDioptria() {
        return dioptria;
    }

    public void setDioptria(Double dioptria) {
        if(dioptria <= 10.0 && dioptria >= -10.0){
            this.dioptria = dioptria;
        }
    }
    
    public Double getCilinder() {
        return cilinder;
    }

    public void setCilinder(Double cilinder) {
        if(cilinder <= 10.0 && cilinder >= -10.0){
            this.cilinder = cilinder;
        }
    }

    public Integer getTengely() {
        return tengely;
    }

    public void setTengely(Integer tengely) {
        if(tengely >= 0 && tengely <= 180){
            this.tengely = tengely;
        }
    }
    
}
